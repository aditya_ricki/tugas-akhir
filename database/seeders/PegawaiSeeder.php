<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Employee;
use Illuminate\Support\Facades\Hash;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userStaffTeknis = User::create([
			'username' => 'ricki',
			'email'    => 'adityaric73@gmail.com',
			'password' => Hash::make('password'),
			'role'     => env('ROLE2'),
        ]);

        Employee::create([
			'first_name' => 'Ricki',
			'last_name'  => '',
			'phone'      => '089765625345',
			'whatsapp'   => '089765625345',
			'address'    => 'Ngawen',
			'photo'      => '.',
			'user_id'    => $userStaffTeknis->id,
        ]);

        $userMarketing = User::create([
			'username' => 'melasintiya',
			'email'    => 'melasintiya@gmail.com',
			'password' => Hash::make('password'),
			'role'     => env('ROLE3'),
        ]);

        Employee::create([
			'first_name' => 'Mela',
			'last_name'  => 'Sintiya',
			'phone'      => '085675425345',
			'whatsapp'   => '085675425345',
			'address'    => 'Bejono',
			'photo'      => '.',
			'user_id'    => $userMarketing->id,
        ]);

        $userFinance = User::create([
			'username' => 'linaseptiana',
			'email'    => 'linaseptiana@gmail.com',
			'password' => Hash::make('password'),
			'role'     => env('ROLE4'),
        ]);

        Employee::create([
			'first_name' => 'Lina',
			'last_name'  => 'Septiana',
			'phone'      => '0857814235345',
			'whatsapp'   => '0857814235345',
			'address'    => 'Watusigar',
			'photo'      => '.',
			'user_id'    => $userFinance->id,
        ]);
    }
}

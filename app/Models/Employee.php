<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function schedules()
    {
        return $this->belongsToMany(Schedule::class);
    }

    public function getNameAttribute()
    {
    	return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscribe extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function customer()
    {
    	return $this->belongsTo(Customer::class);
    }

    public function packet()
    {
    	return $this->hasOne(Packet::class, 'id', 'packet_id');
    }

    public function schedules()
    {
    	return $this->hasMany(Schedule::class);
    }

    public function getDateDiffAttribute()
    {
    	return dateDiff($this->attributes['created_at']);
    }
}

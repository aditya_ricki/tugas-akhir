<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Packet extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = [
    	'price_formated',
    ];

    public function getPriceFormatedAttribute()
    {
    	return shortMoney($this->attributes['price']);
    }
}

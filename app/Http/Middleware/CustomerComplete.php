<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CustomerComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();

        if (!$user->customer) {
            return redirect()->route('customer.profile')->with('error', 'Lengkapi data diri terlebih dahulu');
        }

        return $next($request);
    }
}

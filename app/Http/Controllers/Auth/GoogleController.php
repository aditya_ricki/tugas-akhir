<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;

class GoogleController extends Controller
{
    public function redirect(Request $request)
    {
    	return Socialite::driver('google')->stateless()->redirect();
    }

    public function callback(Request $request)
    {
		try {
			$user   = Socialite::driver('google')->stateless()->user();
			$search = User::where('socialite_id', $user->id)->orWhere('email', $user->email)->first();

			if (!$search) {
				$search = User::create([
					'username'     => $user->name,
					'email'        => $user->email,
					'socialite_id' => $user->id,
					'auth_type'    => 'google',
					'password'     => hashString($user->name),
					'role'         => env('ROLE1'),
				]);
			}

			Auth::login($search);

			return redirect()->route('landing-page');
	    } catch (\Throwable $th) {
	    	\Log::error($th->getMessage());
	    	abort(500);
	    }
    }
}

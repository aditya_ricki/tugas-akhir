<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
	function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function index()
	{
        try {
			$id   = $this->request->user()->id;
			$user = User::with('customer')
        			->where('id', $id)
        			->first();

    		return view('dashboard-customer.profile', compact('user'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function update()
	{
        $this->request->validate([
			'username'   => 'required',
			'email'      => 'required|email',
			'password'   => 'required|confirmed|min:8',
			'first_name' => 'required',
			'last_name'  => 'required',
			'phone'      => 'required|numeric',
			'whatsapp'   => 'required|numeric',
			'photo'      => 'mimes:png,jpg',
			'id_card'    => 'mimes:png,jpg',
			'address'    => 'required',
			'longitude'  => 'required',
			'latitude'   => 'required',
        ]);

        try {
			$user       = User::with('customer')->where('id', $this->request->user()->id)->first();
			$customer   = $user->customer;
			$pathPhoto  = '';
			$pathIdCard = '';

        	$user->update([
				'username' => $this->request->username,
				'email'    => $this->request->email,
				'password' => Hash::make($this->request->pasword),
        	]);

        	if ($customer) {
				$pathPhoto  = $customer->photo;
				$pathIdCard = $customer->id_card;
        	}

        	if ($this->request->file('photo')) {
        		$pathPhoto = uploadFile($this->request->file('photo'), 'customer/photo');
        	}

        	if ($this->request->file('id_card')) {
        		$pathIdCard = uploadFile($this->request->file('id_card'), 'customer/id-card');
    		}

    		$data = [
				'first_name' => $this->request->first_name,
				'last_name'  => $this->request->last_name,
				'phone'      => $this->request->phone,
				'whatsapp'   => $this->request->whatsapp,
				'photo'      => $pathPhoto,
				'id_card'    => $pathIdCard,
				'address'    => $this->request->address,
				'latitude'   => $this->request->latitude,
				'longitude'  => $this->request->longitude,
				'user_id'    => $user->id,
			];

    		if ($customer) {
    			$customer->update($data);
    		} else {
    			Customer::create($data);
    		}

    		return redirect()->back()->with('success', 'Berhasil update profile');;
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}
}

<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        try {
    		return view('dashboard-customer.index');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subscribe;

class SubscribeController extends Controller
{
	function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function store($id)
	{
		try {
			$customer = $this->request->user()->customer;
			$cek      = Subscribe::where('customer_id', $customer->id)->first();

			if ($cek) {
				return redirect()->back()->with('error', 'Anda sudah berlangganan, silahkan pengajuan updrade / downgrade paket');
			}

			Subscribe::create([
				'status'      => env('SUB2'),
				'customer_id' => $customer->id,
				'packet_id'   => $id,
			]);

            return redirect()->back()->with('success', 'Pengajuan pemasangan berhasil dibuat');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}
}

<?php

namespace App\Http\Controllers\StaffTeknis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Packet;

class PacketController extends Controller
{
	function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function index()
	{
        try {
            $packets = Packet::all();

            return view('dashboard-staff-teknis.packet.index', compact('packets'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function create()
	{
        try {
            return view('dashboard-staff-teknis.packet.create');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function store()
	{
		$this->request->validate([
			'name'        => 'required',
			'price'       => 'required|numeric|min:1',
			'description' => 'required',
		]);

        try {
        	Packet::create([
				'name'        => $this->request->name,
				'price'       => $this->request->price,
				'description' => $this->request->description,
        	]);

            return redirect()->route('staff-teknis.packet')->with('success', 'Berhasil menambah paket internet');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function edit($id)
	{
        try {
        	$packet = Packet::findOrFail($id);

            return view('dashboard-staff-teknis.packet.edit', compact('packet'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function update($id)
	{
		$this->request->validate([
			'name'        => 'required',
			'price'       => 'required|numeric|min:1',
			'description' => 'required',
		]);

        try {
        	Packet::findOrFail($id)->update([
				'name'        => $this->request->name,
				'price'       => $this->request->price,
				'description' => $this->request->description,
        	]);

            return redirect()->route('staff-teknis.packet')->with('success', 'Berhasil mengubah paket internet');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function destroy($id)
	{
        try {
        	Packet::findOrFail($id)->delete();

            return redirect()->route('staff-teknis.packet')->with('success', 'Berhasil menghapus paket internet');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}
}

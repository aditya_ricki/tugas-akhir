<?php

namespace App\Http\Controllers\StaffTeknis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Device;

class DeviceController extends Controller
{
	function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function index()
	{
        try {
            $devices = Device::all();

            return view('dashboard-staff-teknis.device.index', compact('devices'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function create()
	{
        try {
            return view('dashboard-staff-teknis.device.create');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function store()
	{
		$this->request->validate([
			'name'        => 'required',
			'price'       => 'required|numeric|min:1',
			'description' => 'required',
		]);

        try {
        	Device::create([
				'name'        => $this->request->name,
				'price'       => $this->request->price,
				'description' => $this->request->description,
        	]);

            return redirect()->route('staff-teknis.device')->with('success', 'Berhasil menambah alat');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function edit($id)
	{
        try {
        	$device = Device::findOrFail($id);

            return view('dashboard-staff-teknis.device.edit', compact('device'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function update($id)
	{
		$this->request->validate([
			'name'        => 'required',
			'price'       => 'required|numeric|min:1',
			'description' => 'required',
		]);

        try {
        	Device::findOrFail($id)->update([
				'name'        => $this->request->name,
				'price'       => $this->request->price,
				'description' => $this->request->description,
        	]);

            return redirect()->route('staff-teknis.device')->with('success', 'Berhasil mengubah alat');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function destroy($id)
	{
        try {
        	Device::findOrFail($id)->delete();

            return redirect()->route('staff-teknis.device')->with('success', 'Berhasil menghapus alat');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}
}

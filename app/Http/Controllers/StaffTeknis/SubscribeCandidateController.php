<?php

namespace App\Http\Controllers\StaffTeknis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subscribe;
use App\Models\Employee;
use App\Models\Device;
use App\Models\Schedule;

class SubscribeCandidateController extends Controller
{
	function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function index()
	{
        try {
            $subscribes = Subscribe::with('customer', 'packet')
            			  ->where('status', env('SUB2'))
            			  ->get();

            return view('dashboard-staff-teknis.subscribe-candidate.index', compact('subscribes'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}

	public function show($id)
	{
        try {
            $subscribe = Subscribe::with('customer', 'packet')
            			 ->where('status', env('SUB2'))
            			 ->first();

            $allEmployee = Employee::whereHas('user', function ($query) {
            	$query->where('role', env('ROLE2'));
            })->get();

            $allDevice = Device::all();

            if (!$subscribe) {
            	return redirect()->back()->with('error', 'Data tidak ditemukan');
            }

            return view('dashboard-staff-teknis.subscribe-candidate.show', compact('subscribe'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}
}

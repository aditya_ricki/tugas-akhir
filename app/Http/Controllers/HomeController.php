<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Packet;

class HomeController extends Controller
{
	function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function index()
	{
        try {
            $packets = Packet::all();

            return view('home', compact('packets'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
	}
}

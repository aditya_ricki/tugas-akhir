<?php

namespace App\Http\Controllers\Marketing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        try {
    		return view('dashboard-marketing.index');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
    }
}

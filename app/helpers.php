<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

function writeLog($message)
{
    Log::error($message);
    abort(500);
}

function uploadFile($file, $path, $isBase64 = false)
{
	try {
		if ($isBase64) {
			$path = str_replace('_', '-', $path) . '/';
		    $file = base64_decode(str_replace('base64,', '', explode(';', $file)[1]), true);
		    $name = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 15) . '.webp';

		    Storage::disk(env('STORAGE', 'local'))->put('public/' . $path . $name, $file);

	    	return 'storage/' . $path . $name;
		} else {
			$path    = str_replace('_', '-', $path);
			$ext     = $file->getClientOriginalExtension();
			$newFile = Storage::disk(env('STORAGE', 'local'))->put('public/' . $path, $file);
			$arr     = explode('/', $newFile);

	    	return 'storage/' . $path . '/' . end($arr);
		}
	} catch (\Throwable $th) {
	    Log::error($th->getMessage());
	    abort(500);
	}
}

function deleteFile($path)
{
	try {
    	return unlink(public_path($path));
	} catch (\Throwable $th) {
	    Log::error($th->getMessage());
	    abort(500);
	}
}

function hashString($string)
{
	return Hash::make($string);
}

function greeting()
{
	$hours = Carbon::now()->format('H');

	if ($hours <= 9) {
		$greeting = 'Selamat Pagi';
	} elseif ($hours <= 15) {
		$greeting = 'Selamat Siang';
	} elseif ($hours <= 18) {
		$greeting = 'Selamat Sore';
	} else {
		$greeting = 'Selamat Malam';
	}

	return $greeting;
}

function dateDiff($tanggal)
{
	return Carbon::parse($tanggal)->diffForHumans();
}

function shortMoney($n, $presisi = 1) {
	if ($n < 90) {
		$format_angka = number_format($n, $presisi);
		$simbol = 'rupiah';
	} else if ($n < 900) {
		$format_angka = number_format($n / 100, $presisi);
		$simbol = 'ratus';
	} else if ($n < 900000) {
		$format_angka = number_format($n / 1000, $presisi);
		$simbol = 'k';
	} else if ($n < 900000000) {
		$format_angka = number_format($n / 1000000, $presisi);
		$simbol = 'jt';
	} else if ($n < 900000000000) {
		$format_angka = number_format($n / 1000000000, $presisi);
		$simbol = 'M';
	} else {
		$format_angka = number_format($n / 1000000000000, $presisi);
		$simbol = 'T';
	}

	if ( $presisi > 0 ) {
		$pisah = '.' . str_repeat( '0', $presisi );
		$format_angka = str_replace( $pisah, '', $format_angka );
	}

	return $format_angka . $simbol;
}

function numberInterval($angka, $interval = 5)
{
	$result = [];

	for ($i = 1; $i <= $interval; $i++) {
		$item = [
			'alias' => shortMoney(round($angka / $i)),
			'value' => round($angka / $i, 2),
		];

		array_push($result, $item);
	}

	return $result;
}

@extends('layouts.app')

@section('title', 'Heles Media Network')

@section('main-content')
<div class="header bg-primary pt-5 pb-7">
    <div class="container">
        <div class="header-body">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="pr-5">
                        <h1 class="display-2 text-white font-weight-bold mb-0">Heles Media Network</h1>
                        <h2 class="display-4 text-white font-weight-light">A beautiful premium dashboard for Bootstrap 4.</h2>
                        <p class="text-white mt-4">Argon perfectly combines reusable HTML and modular CSS with a modern styling and beautiful markup throughout each HTML template in the pack.</p>
                        <div class="mt-5">
                            <a href="./pages/dashboards/dashboard.html" class="btn btn-neutral my-2">Explore Dashboard</a>
                            <a href="https://www.creative-tim.com/product/argon-dashboard-pro" class="btn btn-default my-2">Purchase now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row pt-5">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow mb-4">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                    <h5 class="h3">Components</h5>
                                    <p>Argon comes with over 70 handcrafted components.</p>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow mb-4">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                    <h5 class="h3">Plugins</h5>
                                    <p>Fully integrated and extendable third-party plugins that you will love.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pt-lg-5 pt-4">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow mb-4">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                    <h5 class="h3">Pages</h5>
                                    <p>From simple to complex, you get a beautiful set of 15+ page examples.</p>
                                </div>
                            </div>
                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="icon icon-shape bg-gradient-warning text-white rounded-circle shadow mb-4">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                    <h5 class="h3">Documentation</h5>
                                    <p>You will love how easy is to to work with Argon.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
    </div>
</div>

<section class="py-6 pb-9 bg-default" style="overflow: hidden;">
    <div class="row justify-content-center text-center">
        <div class="col-md-6">
            <h2 class="display-3 text-white">A complete HTML solution</h2>
            <p class="lead text-white">
                Argon is a completly new product built on our newest re-built from scratch framework structure that is meant to make our products more intuitive, more adaptive and, needless to say, so much easier to customize. Let Argon amaze you with its cool features and build tools and get your project to a whole new level.
            </p>
        </div>
    </div>
</section>

<section class="section section-lg pt-lg-0 mt--7">
    <div class="container">
        <div class="row mb-3 text-center">
            @foreach($packets as $packet)
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="card mb-4 box-shadow">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">{{ $packet->name }}</h4>
                    </div>
                    <div class="card-body">
                        <h1 class="card-title pricing-card-title">{{ $packet->price_formated }} <small class="text-muted">/ bln</small></h1>
                        <p class="mt-3 mb-4">
                            {!! $packet->description !!}
                        </p>
                        <a href="{{ route('customer.subscribe.store', $packet->id) }}" class="btn btn-lg btn-block btn-primary">Purchase now</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<section class="py-6">
    <div class="container">
        <div class="row row-grid align-items-center">
            <div class="col-md-6 order-md-2">
                <img src="{{ asset('img/theme/landing-1.png') }}" class="img-fluid">
            </div>
            <div class="col-md-6 order-md-1">
                <div class="pr-md-5">
                    <h1>Awesome features</h1>
                    <p>This dashboard comes with super cool features that are meant to help in the process. Handcrafted components, page examples and functional widgets are just a few things you will see and love at first sight.</p>
                    <ul class="list-unstyled mt-5">
                        <li class="py-2">
                            <div class="d-flex align-items-center">
                                <div>
                                    <div class="badge badge-circle badge-success mr-3">
                                        <i class="ni ni-settings-gear-65"></i>
                                    </div>
                                </div>
                                <div>
                                    <h4 class="mb-0">Carefully crafted components</h4>
                                </div>
                            </div>
                        </li>
                        <li class="py-2">
                            <div class="d-flex align-items-center">
                                <div>
                                    <div class="badge badge-circle badge-success mr-3">
                                        <i class="ni ni-html5"></i>
                                    </div>
                                </div>
                                <div>
                                    <h4 class="mb-0">Amazing page examples</h4>
                                </div>
                            </div>
                        </li>
                        <li class="py-2">
                            <div class="d-flex align-items-center">
                                <div>
                                    <div class="badge badge-circle badge-success mr-3">
                                        <i class="ni ni-satisfied"></i>
                                    </div>
                                </div>
                                <div>
                                    <h4 class="mb-0">Super friendly support team</h4>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-6">
    <div class="container">
        <div class="row row-grid align-items-center">
            <div class="col-md-6">
                <img src="{{ asset('img/theme/landing-2.png') }}" class="img-fluid">
            </div>
            <div class="col-md-6">
                <div class="pr-md-5">
                    <h1>Example pages</h1>
                    <p>If you want to get inspiration or just show something directly to your clients, you can jump start your development with our pre-built example pages.</p>
                    <a href="./pages/examples/profile.html" class="font-weight-bold text-warning mt-5">Explore pages</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-6">
    <div class="container">
        <div class="row row-grid align-items-center">
            <div class="col-md-6 order-md-2">
                <img src="{{ asset('img/theme/landing-3.png') }}" class="img-fluid">
            </div>
            <div class="col-md-6 order-md-1">
                <div class="pr-md-5">
                    <h1>Lovable widgets and cards</h1>
                    <p>We love cards and everybody on the web seems to. We have gone above and beyond with options for you to organise your information. From cards designed for content, to pricing cards or user profiles, you will have many options to choose from.</p>
                    <a href="./pages/widgets.html" class="font-weight-bold text-info mt-5">Explore widgets</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

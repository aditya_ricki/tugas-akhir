@extends('layouts.dashboard.app')

@section('title', 'Profile Customer | HMN')

@section('main-content')
<div class="header pb-6 d-flex align-items-center" style="min-height: 500px; background-image: url({{ $user->customer ? asset($user->customer->photo) : asset('img/theme/profile-cover-2.jpg') }}); background-size: cover; background-position: center top;">
  <span class="mask bg-gradient-default opacity-8"></span>
  <div class="container-fluid d-flex align-items-center">
    <div class="row">
      <div class="col-lg-7 col-md-10">
        <h1 class="display-2 text-white">{{ greeting() }}, {{ $user->customer ? $user->customer->name : 'Pelanggan' }}</h1>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-4 order-xl-2">
      <div class="card card-profile">
        <img src="{{ asset('img/theme/img-1-1000x600.jpg') }}" alt="Image placeholder" class="card-img-top">
        <div class="row justify-content-center">
          <div class="col-lg-3 order-lg-2">
            <div class="card-profile-image">
              <a href="#">
                <img src="{{ $user->customer ? asset($user->customer->photo) : asset('img/theme/profile-cover-2.jpg') }}" class="rounded-circle">
              </a>
            </div>
          </div>
        </div>
        <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
          <div class="d-flex justify-content-between">
          </div>
        </div>
        <div class="card-body pt-0">
          <div class="text-center">
            <h5 class="h3">
              {{ $user->customer ? $user->customer->name : 'Pelanggan' }}
            </h5>
            <div class="h5 font-weight-300">
              <i class="ni location_pin mr-2"></i>{{ $user->customer ? $user->customer->address : '-' }}
            </div>
            <div class="h5 mt-4">
              <i class="ni business_briefcase-24 mr-2"></i>{{ $user->email }}
            </div>
            <div>
              <i class="ni education_hat mr-2"></i>{{ $user->customer ? $user->customer->phone : '-' }}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-8 order-xl-1">
      <div class="row">
        <div class="col-lg-6">
          <div class="card bg-gradient-info border-0">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0 text-white">Total traffic</h5>
                  <span class="h2 font-weight-bold mb-0 text-white">350,897</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                    <i class="ni ni-active-40"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-white mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                <span class="text-nowrap text-light">Since last month</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card bg-gradient-danger border-0">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0 text-white">Performance</h5>
                  <span class="h2 font-weight-bold mb-0 text-white">49,65%</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                    <i class="ni ni-spaceship"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-white mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                <span class="text-nowrap text-light">Since last month</span>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="card">
        <form action="{{ route('customer.profile.update') }}" method="post" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <div class="card-header">
            <div class="row align-items-center">
              <div class="col-12">
                <h3 class="mb-0">Edit profile </h3>
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
              </div>
            </div>
          </div>
          <div class="card-body">
            <h6 class="heading-small text-muted mb-4">Informasi Login</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Username</label>
                    <input type="text" id="input-username" class="form-control" placeholder="Username" value="{{ $user->username }}" name="username">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-email">Email</label>
                    <input type="email" id="input-email" class="form-control" placeholder="Email" value="{{ $user->email }}" name="email">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-password">Password</label>
                    <input type="password" id="input-password" class="form-control" name="password">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-konfirmasi-password">Konfirmasi Password</label>
                    <input type="password" id="input-konfirmasi-password" class="form-control" name="password_confirmation">
                  </div>
                </div>
              </div>
            </div>
            <hr class="my-4" />
            <!-- Address -->
            <h6 class="heading-small text-muted mb-4">Detail Pelanggan</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-first-name">Nama Depan</label>
                    <input type="text" id="input-first-name" class="form-control" placeholder="Nama Depan" value="{{ $user->customer ? $user->customer->first_name : '' }}" name="first_name">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-last-name">Nama Belakang</label>
                    <input type="text" id="input-last-name" class="form-control" placeholder="Nama Belakang" value="{{ $user->customer ? $user->customer->last_name : '' }}" name="last_name">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-phone">No Telepon</label>
                    <input type="text" id="input-phone" class="form-control" placeholder="No Telepon" value="{{ $user->customer ? $user->customer->phone : '' }}" name="phone">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-whatsapp">No Whatsapp</label>
                    <input type="text" id="input-whatsapp" class="form-control" placeholder="No Whatsapp" value="{{ $user->customer ? $user->customer->whatsapp : '' }}" name="whatsapp">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-photo">Foto Profile</label>
                    <input type="file" id="input-photo" class="form-control" name="photo">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-id-card">Foto KTP/SIM</label>
                    <input type="file" id="input-id-card" class="form-control" name="id_card">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Alamat</label>
                    <textarea rows="4" class="form-control" placeholder="Alamat" name="address">{{ $user->customer ? $user->customer->address : '' }}</textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-latitude">Latitude</label>
                    <input type="text" id="input-latitude" class="form-control" placeholder="Latitude" value="{{ $user->customer ? $user->customer->latitude : '' }}" name="latitude">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-longitude">Longitude</label>
                    <input type="text" id="input-longitude" class="form-control" placeholder="Longitude" value="{{ $user->customer ? $user->customer->longitude : '' }}" name="longitude">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div style="width: 100%; height: 480px" id="map-container"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-12">
                  <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
                  <a href="{{ route('dashboard.customer') }}" class="btn btn-sm btn-default">Batal</a>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
<script src="https://js.api.here.com/v3/3.1/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-ui.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
<script>
  $(document).ready(function () {
    navigator.geolocation.getCurrentPosition(function(position) {
      let lastLong = document.getElementById('input-longitude').value
      let lastLat = document.getElementById('input-latitude').value

      let lat  = lastLat ? lastLat : position.coords.latitude
      let long = lastLong ? lastLong : position.coords.longitude

      function addDraggableMarker(peta, behavior){
        // create marker
        let marker = new H.map.Marker({
          lat: lat,
          lng: long
        },
        {
          // mark the object as volatile for the smooth dragging
          volatility: true
        })

        // Add the marker to the map and center the map at the location of the marker:
        peta.addObject(marker)

        // Ensure that the marker can receive drag events
        marker.draggable = true
        peta.addObject(marker)

        // disable the default draggability of the underlying map
        // and calculate the offset between mouse and target's position
        // when starting to drag a marker object:
        peta.addEventListener('dragstart', function(ev) {
          let target = ev.target
          let pointer = ev.currentPointer

          if (target instanceof H.map.Marker) {
            let targetPosition = peta.geoToScreen(target.getGeometry())
            target['offset'] = new H.math.Point(pointer.viewportX - targetPosition.x, pointer.viewportY - targetPosition.y)
            behavior.disable()
          }
        }, false)

        // re-enable the default draggability of the underlying map
        // when dragging has completed
        peta.addEventListener('dragend', function(ev) {
          let target = ev.target
          document.getElementById('input-longitude').value = target.b.lng
          document.getElementById('input-latitude').value = target.b.lat

          if (target instanceof H.map.Marker) {
            behavior.enable()
          }
        }, false)

        // Listen to the drag event and move the position of the marker
        // as necessary
        peta.addEventListener('drag', function(ev) {
          let target = ev.target
          let pointer = ev.currentPointer
          if (target instanceof H.map.Marker) {
            target.setGeometry(peta.screenToGeo(pointer.viewportX - target['offset'].x, pointer.viewportY - target['offset'].y))
          }
        }, false)
      }

      let platform = new H.service.Platform({
        'apikey': '{{ env('HEREMAPS_API_KEY') }}'
      })

      // Obtain the default map types from the platform object:
      let defaultLayers = platform.createDefaultLayers()

      // Instantiate (and display) a map object:
      let peta = new H.Map(
        document.getElementById('map-container'),
        defaultLayers.vector.normal.map,
        {
          zoom: 15,
          center: {
            lat: lat,
            lng: long
          }
        }
      )

      // add a resize listener to make sure that the map occupies the whole container
      window.addEventListener('resize', () => peta.getViewPort().resize())

      // Add map events functionality to the map
      let mapEvents = new H.mapevents.MapEvents(peta)

      // Add behavior to the map: panning, zooming, dragging.
      let behavior = new H.mapevents.Behavior(mapEvents)

      // map ui
      let ui = H.ui.UI.createDefault(peta, defaultLayers)

      // Add the click event listener
      addDraggableMarker(peta, behavior)
    })
  })
</script>
@endsection

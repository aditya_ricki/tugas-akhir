@extends('layouts.dashboard.app')

@section('title', 'Customers | HMN')

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Customers</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('landing-page') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('dashboard.marketing') }}">Dashboards</a></li>
              <li class="breadcrumb-item active" aria-current="page">Customers</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="{{ route('marketing.customer.create') }}" class="btn btn-sm btn-neutral"><i class="fas fa-plus"></i> Tambah</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover my-datatable">
              <thead>
                <th>No</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Telepon</th>
                <th>Whatsapp</th>
                <th></th>
              </thead>
              <tbody>
                @foreach($customers as $customer)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $customer->name }}</td>
                  <td>{{ $customer->email }}</td>
                  <td>{{ $customer->customer ? $customer->customer->phone : '-' }}</td>
                  <td>{{ $customer->customer ? $customer->customer->whatsapp : '-' }}</td>
                  <td>
                    <a href="{{ route('marketing.customer.show', $customer->id) }}" class="btn btn-primary btn-sm text-white">
                      <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('marketing.customer.edit', $customer->id) }}" class="btn btn-warning btn-sm text-white">
                      <i class="fas fa-edit"></i>
                    </a>
                    <button class="btn btn-danger btn-sm text-white">
                      <i class="fas fa-trash"></i>
                    </button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

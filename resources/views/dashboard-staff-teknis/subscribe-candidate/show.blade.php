@extends('layouts.dashboard.app')

@section('title', 'Subscribe | HMN')

@section('custom-css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Detail Calon Pelanggan</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('landing-page') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('staff-teknis.subscribe-candidate') }}">Calon Pelanggan</a></li>
              <li class="breadcrumb-item active" aria-current="page">Detail</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="{{ route('staff-teknis.subscribe-candidate') }}" class="btn btn-sm btn-neutral"><i class="fas fa-arrow-left"></i> Kembali</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-6 col-md-6 col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row mb-3">
            <div class="col-md-4 col-sm-4">Nama</div>
            <div class="col-md-8 col-sm-8">: {{ $subscribe->customer->name }}</div>
          </div>
          <div class="row mb-3">
            <div class="col-md-4 col-sm-4">No HP</div>
            <div class="col-md-8 col-sm-8">: {{ $subscribe->customer->phone }}</div>
          </div>
          <div class="row mb-3">
            <div class="col-md-4 col-sm-4">Whatsapp</div>
            <div class="col-md-8 col-sm-8">: {{ $subscribe->customer->whatsapp }}</div>
          </div>
          <div class="row mb-3">
            <div class="col-md-4 col-sm-4">Foto</div>
            <div class="col-md-8 col-sm-8">
              <a data-fancybox="gallery" href="{{ asset($subscribe->customer->photo) }}">
                <img class="img-thumbnail" src="{{ asset($subscribe->customer->photo) }}" alt="Foto" width="200">
              </a>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-4 col-sm-4">Foto KTP</div>
            <div class="col-md-8 col-sm-8">
              <a data-fancybox="gallery" href="{{ asset($subscribe->customer->id_card) }}">
                <img class="img-thumbnail" src="{{ asset($subscribe->customer->id_card) }}" alt="Foto KTP" width="200">
              </a>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-4 col-sm-4">Nama Paket</div>
            <div class="col-md-8 col-sm-8">: {{ $subscribe->packet->name }}</div>
          </div>
          <div class="row mb-3">
            <div class="col-md-4 col-sm-4">Harga</div>
            <div class="col-md-8 col-sm-8">: {{ $subscribe->packet->price }}</div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-6 col-md-6 col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="width: 100%; height: 480px" id="map-container"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
        <div class="card-body">
          <form action="#" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-name">Nama</label>
                  <input type="text" id="input-name" class="form-control" placeholder="Nama" value="{{ '' }}" name="name">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-price">Harga</label>
                  <input type="number" id="input-price" class="form-control" placeholder="Harga" value="{{ '' }}" name="price" min="1">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label">Deskripsi</label>
                  <textarea rows="4" class="form-control my-textarea" placeholder="Deskripsi" name="description">{{ '' }}</textarea>
                </div>
              </div>
            </div>
            <div class="row mt-3">
              <div class="col-md-12 col-sm-12">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{ route('staff-teknis.subscribe-candidate') }}" class="btn btn-secondary">Batal</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-ui.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
<script>
  $(document).ready(function () {
    navigator.geolocation.getCurrentPosition(function(position) {
      let lat  = '{{ $subscribe->customer->latitude }}'
      let long = '{{ $subscribe->customer->longitude }}'

      function addDraggableMarker(peta, behavior){
        // create marker
        let marker = new H.map.Marker({
          lat: lat,
          lng: long
        },
        {
          // mark the object as volatile for the smooth dragging
          volatility: true
        })

        // Add the marker to the map and center the map at the location of the marker:
        peta.addObject(marker)

        // Ensure that the marker can receive drag events
        marker.draggable = true
        peta.addObject(marker)

        // disable the default draggability of the underlying map
        // and calculate the offset between mouse and target's position
        // when starting to drag a marker object:
        peta.addEventListener('dragstart', function(ev) {
          let target = ev.target
          let pointer = ev.currentPointer

          if (target instanceof H.map.Marker) {
            let targetPosition = peta.geoToScreen(target.getGeometry())
            target['offset'] = new H.math.Point(pointer.viewportX - targetPosition.x, pointer.viewportY - targetPosition.y)
            behavior.disable()
          }
        }, false)

        // re-enable the default draggability of the underlying map
        // when dragging has completed
        peta.addEventListener('dragend', function(ev) {
          let target = ev.target
          // document.getElementById('input-longitude').value = target.b.lng
          // document.getElementById('input-latitude').value = target.b.lat

          if (target instanceof H.map.Marker) {
            behavior.enable()
          }
        }, false)

        // Listen to the drag event and move the position of the marker
        // as necessary
        peta.addEventListener('drag', function(ev) {
          let target = ev.target
          let pointer = ev.currentPointer
          if (target instanceof H.map.Marker) {
            target.setGeometry(peta.screenToGeo(pointer.viewportX - target['offset'].x, pointer.viewportY - target['offset'].y))
          }
        }, false)
      }

      let platform = new H.service.Platform({
        'apikey': '{{ env('HEREMAPS_API_KEY') }}'
      })

      // Obtain the default map types from the platform object:
      let defaultLayers = platform.createDefaultLayers()

      // Instantiate (and display) a map object:
      let peta = new H.Map(
        document.getElementById('map-container'),
        defaultLayers.vector.normal.map,
        {
          zoom: 15,
          center: {
            lat: lat,
            lng: long
          }
        }
      )

      // add a resize listener to make sure that the map occupies the whole container
      window.addEventListener('resize', () => peta.getViewPort().resize())

      // Add map events functionality to the map
      let mapEvents = new H.mapevents.MapEvents(peta)

      // Add behavior to the map: panning, zooming, dragging.
      let behavior = new H.mapevents.Behavior(mapEvents)

      // map ui
      let ui = H.ui.UI.createDefault(peta, defaultLayers)

      // Add the click event listener
      addDraggableMarker(peta, behavior)
    })
  })
</script>
@endsection

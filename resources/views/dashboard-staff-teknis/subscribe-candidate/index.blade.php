@extends('layouts.dashboard.app')

@section('title', 'Subscribe | HMN')

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Calon Pelanggan</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('landing-page') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('dashboard.staff-teknis') }}">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Calon Pelanggan</li>
            </ol>
          </nav>
        </div>
        {{-- <div class="col-lg-6 col-5 text-right">
          <a href="{{ route('staff-teknis.packet.create') }}" class="btn btn-sm btn-neutral"><i class="fas fa-plus"></i> Tambah</a>
        </div> --}}
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover my-datatable">
              <thead>
                <th>No</th>
                <th>Nama</th>
                <th>Hp / Whatsapp</th>
                <th>Paket</th>
                <th>Tanggal</th>
                <th></th>
              </thead>
              <tbody>
                @foreach($subscribes as $subscribe)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $subscribe->customer->name }}</td>
                  <td>{{ $subscribe->customer->phone }} / {{ $subscribe->customer->whatsapp }}</td>
                  <td>{{ $subscribe->packet->name }}</td>
                  <td>{{ $subscribe->date_diff }}</td>
                  <td>
                    <form action="{{ route('staff-teknis.subscribe-candidate.destroy', $subscribe->id) }}" method="post" id="form-{{ $subscribe->id }}">
                      @csrf
                      @method('DELETE')
                      <a href="{{ route('staff-teknis.subscribe-candidate.show', $subscribe->id) }}" class="btn btn-success btn-sm text-white">
                        <i class="fas fa-eye"></i>
                      </a>
                      <button type="button" class="btn btn-danger btn-sm text-white btn-hapus" data-id="{{ $subscribe->id }}">
                        <i class="fas fa-trash"></i>
                      </button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
<script>
  $('.btn-hapus').on('click', function (e) {
    let id = e.target.dataset['id']

    Swal.fire({
      title: 'Peringatan',
      text: 'Yakin mau hapus data ini?',
      icon: 'warning',
      showCancelButton: true,
    }).then((data) => {
      if (data.isConfirmed) {
        $(`#form-${id}`).submit()
      }
    })
  })
</script>
@endsection
